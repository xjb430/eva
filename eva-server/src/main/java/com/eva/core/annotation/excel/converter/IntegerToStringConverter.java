package com.eva.core.annotation.excel.converter;

import com.eva.core.annotation.excel.ExcelDataConverterAdapter;

/**
 * 整数转字符串
 * @author Eva.Caesar Liu
 * @since 2022/05/09 19:56
 */
public class IntegerToStringConverter implements ExcelDataConverterAdapter {

    /**
     * 转换
     *
     * @param args 参数集合
     * - args[0] 单元格数据
     */
    @Override
    public Object convert(Object... args) {
        Object value = args[0];
        if (value == null) {
            return null;
        }
        if (value instanceof String) {
            return value;
        }
        return "" + ((Double)value).longValue();
    }
}
