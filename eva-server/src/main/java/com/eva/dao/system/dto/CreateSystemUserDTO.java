package com.eva.dao.system.dto;

import com.eva.dao.system.model.SystemUser;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author Eva.Caesar Liu
 * @since 2022/05/09 19:56
 */
@Data
@ApiModel("创建用户参数")
public class CreateSystemUserDTO extends SystemUser {
}
