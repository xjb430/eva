package com.eva.dao.system.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author Eva.Caesar Liu
 * @since 2022/05/09 19:56
 */
@Data
@ApiModel("删除权限参数")
public class DeleteSystemPermissionDTO {

    @ApiModelProperty("权限ID")
    private Integer id;

    @ApiModelProperty("模块前缀")
    private String modulePrefix;
}
