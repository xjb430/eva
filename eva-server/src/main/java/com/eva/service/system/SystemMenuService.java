package com.eva.service.system;

import com.eva.dao.system.model.SystemMenu;
import com.eva.dao.system.vo.SystemMenuListVO;

import java.util.List;

/**
 * 系统菜单Service定义
 * @author Eva.Caesar Liu
 * @since 2022/05/09 19:56
 */
public interface SystemMenuService {

    /**
     * 创建
     *
     * @param systemMenu 实体
     * @return Integer
     */
    Integer create(SystemMenu systemMenu);

    /**
     * 主键删除
     *
     * @param id 主键
     */
    void deleteById(Integer id);

    /**
     * 批量主键删除
     *
     * @param ids 主键列表
     */
    void deleteByIdInBatch(List<Integer> ids);

    /**
     * 主键更新
     *
     * @param systemMenu 实体
     */
    void updateById(SystemMenu systemMenu);

    /**
     * 批量主键更新
     *
     * @param systemMenus 实体
     */
    void updateByIdInBatch(List<SystemMenu> systemMenus);

    /**
     * 主键查询
     *
     * @param id 主键
     * @return SystemMenu
     */
    SystemMenu findById(Integer id);

    /**
     * 条件查询单条记录
     *
     * @param systemMenu 查询条件
     * @return SystemMenu
     */
    SystemMenu findOne(SystemMenu systemMenu);

    /**
     * 条件查询
     *
     * @param systemMenu 查询条件
     * @return List<SystemMenu>
     */
    List<SystemMenu> findList(SystemMenu systemMenu);

    /**
     * 查询列表
     *
     * @return List<SystemMenuListVO>
     */
    List<SystemMenuListVO> findList();

    /**
     * 查询一级菜单列表
     *
     * @return List<SystemMenu>
     */
    List<SystemMenu> findRootList();

    /**
     * 查询用户ID查询
     *
     * @param userId 用户ID
     * @return List<SystemMenu>
     */
    List<SystemMenu> findByUserId(Integer userId);

    /**
     * 根据角色ID查询
     *
     * @param roleId 角色ID
     * @return List<SystemMenu>
     */
    List<SystemMenu> findByRoleId(Integer roleId);

    /**
     * 条件统计
     *
     * @param systemMenu 统计参数
     * @return long
     */
    long count(SystemMenu systemMenu);

    /**
     * 查询子菜单
     *
     * @param menuId 菜单ID
     * @return List<Integer>
     */
    List<Integer> findChildren(Integer menuId);
}
