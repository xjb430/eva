package com.eva.core.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 项目属性配置
 * @author Eva.Caesar Liu
 * @since 2022/05/09 19:56
 */
@Data
@Component
@ConfigurationProperties(prefix = "project", ignoreInvalidFields = true)
public class ProjectConfig {

    // 生产环境
    private static final String ENV_PRODUCTION = "production";

    // 测试模式
    private static final String MODE_TESTING = "testing";

    // 模式
    private String mode;

    // 环境
    private String env;

    // 版本
    private String version;

    /**
     * 判断是否为测试模式，当前测试模式下完成以下事项
     * - 不校验验证码
     *
     * @return boolean
     */
    public boolean isTestingMode () {
        // 生产环境无测试模式
        if (ENV_PRODUCTION.equals(env)) {
            return Boolean.FALSE;
        }
        return MODE_TESTING.equalsIgnoreCase(mode);
    }
}
